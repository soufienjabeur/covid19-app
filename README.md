## Project steps

### Client side

- Createa React App using command `npx create-react-app posts_app`
- Installing necessary libraries`npm axios chart.js react-chartjs-2 react-number-format`
  - axios: communicate or send requests to the server side
  - chart.js: simple yet flexible JavaScript charting for designers & developers
  - react-chartjs-2: React wrapper for Chart.js
  - react-number-format: format number in an input or as a text
- Visite the POSTMAN web site where you can get the Covid19 API
- Create new file `axios.js` under the `src` folder, where you can create a new instance of axios with a custom config
- Create new folder `components` inside of it, create the following files:

  - `Card.js`
  - `Card.css`
  - `CovidSumary.js`
  - `CovidSumary.css`
  - `LineGraph.js`

- add few SEO rules using `meta-tags` such as:
  - adding description to the app
  - keyWords Attribute
  - Charset
  - Author
- Deploy the News app on Netlify

#### Hooks: `useEffect()`, `useState()`

### UseFul Links:

[https://documenter.getpostman.com/view/10808728/SzS8rjbc#27454960-ea1c-4b91-a0b6-0468bb4e6712](https://documenter.getpostman.com/view/10808728/SzS8rjbc#27454960-ea1c-4b91-a0b6-0468bb4e6712)

[https://www.netlify.com/](https://www.netlify.com/)
