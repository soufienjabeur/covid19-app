import React from 'react';
import Card from './Card';
import NumberFormat from 'react-number-format';
import './CovidSummary.css';

const CovidSummary = (props) => {

    const { totalConfirmed,
        totalRecovered,
        totalDeaths,
        country } = props;
    return (
        <div>
            <div>
                <h1>{country === '' ? ' World wide COVID-19 report' : country}</h1>
                <div style={{
                    display: 'flex',
                    justifyContent: 'center'
                }}>
                    <Card>
                        <span>Total Confirmed <br /></span>
                        <span>{<NumberFormat value={totalConfirmed} displayType={'text'} thousandSeparator={true} />}
                        </span>
                    </Card>
                    <Card>
                        <span>Total Recovered <br /></span>
                        <span>{<NumberFormat value={totalRecovered} displayType={'text'} thousandSeparator={true} />}</span>
                    </Card>
                    <Card>
                        <span>Total Deaths <br /></span>
                        <span>{<NumberFormat value={totalDeaths} displayType={'text'} thousandSeparator={true} />}</span>
                    </Card>
                </div>
            </div>
        </div>
    )
}

export default CovidSummary;
